package com.example.memberbrowser;

import com.example.memberbrowser.model.RepositorySource;
import com.example.memberbrowser.model.data.Departments;
import com.example.memberbrowser.model.data.Member;
import com.example.memberbrowser.model.remotesource.MembersService;
import com.example.memberbrowser.model.remotesource.RemoteRepository;

import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Set;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by pkuszewski on 01.04.2017.
 */

public class RepositoryTest {


    RepositorySource source;

    @Before
    public void setupTest() {
        Retrofit.Builder builder = new Retrofit.Builder();
        Retrofit retrofit = builder.addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl("http://nielsmouthaan.nl/backbase/")
                .build();
        MembersService service = retrofit.create(MembersService.class);
        source = new RemoteRepository(service);
    }

    @Test
    public void testService() {
        assertEquals(3, source.getAllDepartments().blockingFirst().size());
        assertTrue(source.getAllDepartments().blockingFirst().contains("cxp"));
        assertTrue(source.getAllDepartments().blockingFirst().contains("launchpad"));
        assertTrue(source.getAllDepartments().blockingFirst().contains("mobile"));
        assertFalse(source.getAllDepartments().blockingFirst().contains("unknown"));
    }

    @Test
    public void testMembers() {
        Set<String> departments = source.getAllDepartments().blockingFirst();
        for (String dept : departments) {
            List<Member> members = source.getMembersByDepartmentName(dept).blockingFirst();
            assertNotNull(members);
            assertTrue(members.size() > 0);
            for (Member member : members) {
                assertNotNull(member.getEmail());
                assertNotNull(member.getName());
                assertNotNull(member.getSurname());
                assertNotNull(member.getRole());
                //photo is optional
            }
        }
    }

    @Test
    public void testDepartments() {
        Departments departments = source.getAllMembers().blockingFirst();
        assertNotNull(departments);
        assertNotNull(departments.getCxp());
        assertNotNull(departments.getLaunchpad());
        assertNotNull(departments.getMobile());
    }

}
