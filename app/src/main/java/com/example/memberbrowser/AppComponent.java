package com.example.memberbrowser;

import com.example.memberbrowser.model.RepositoryModule;
import com.example.memberbrowser.model.member.MemberComponent;
import com.example.memberbrowser.model.member.MemberModule;
import com.example.memberbrowser.ui.memberlist.components.MemberListComponent;
import com.example.memberbrowser.ui.memberlist.modules.MemberListModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by pkuszewski on 01.04.2017.
 */
@Component(modules = {AppModule.class, RepositoryModule.class})
@Singleton
public interface AppComponent {
    MemberListComponent plus(MemberListModule module);

    MemberComponent plus(MemberModule memberModule);
}
