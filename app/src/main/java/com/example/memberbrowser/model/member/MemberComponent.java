package com.example.memberbrowser.model.member;

import com.example.memberbrowser.scopes.MemberScope;
import com.example.memberbrowser.ui.memberdetails.MemberDetailsComponent;
import com.example.memberbrowser.ui.memberdetails.MemberDetailsModule;

import dagger.Subcomponent;

/**
 * Created by pkuszewski on 01.04.2017.
 */
@Subcomponent(modules = MemberModule.class)
@MemberScope
public interface MemberComponent {

    MemberDetailsComponent plus(MemberDetailsModule module);

}
