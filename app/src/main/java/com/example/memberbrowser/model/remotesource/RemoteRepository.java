package com.example.memberbrowser.model.remotesource;

import com.example.memberbrowser.model.RepositorySource;
import com.example.memberbrowser.model.data.Departments;
import com.example.memberbrowser.model.data.Member;

import java.util.List;
import java.util.Set;

import io.reactivex.Observable;
import lombok.AllArgsConstructor;

/**
 * Created by pkuszewski on 01.04.2017.
 */
@AllArgsConstructor
public class RemoteRepository implements RepositorySource {

    private MembersService service;

    @Override
    public Observable<Set<String>> getAllDepartments() {
        return service.getDepartments()
                .map(departments -> departments.createMap().keySet());
    }

    @Override
    public Observable<List<Member>> getMembersByDepartmentName(String name) {
        return service.getDepartments()
                .map(departments -> departments.createMap().get(name.toLowerCase()));
    }

    @Override
    public Observable<Departments> getAllMembers() {
        return service.getDepartments();
    }
}
