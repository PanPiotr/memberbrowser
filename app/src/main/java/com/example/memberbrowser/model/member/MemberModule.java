package com.example.memberbrowser.model.member;

import com.example.memberbrowser.model.data.Member;
import com.example.memberbrowser.scopes.MemberScope;

import dagger.Module;
import dagger.Provides;
import lombok.AllArgsConstructor;

/**
 * Created by pkuszewski on 01.04.2017.
 */
@AllArgsConstructor
@MemberScope
@Module
public class MemberModule {

    private Member member;

    @Provides
    @MemberScope
    Member provideMember() {
        return member;
    }

}
