package com.example.memberbrowser.model.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by pkuszewski on 01.04.2017.
 */

public class Departments {

    @Expose
    @SerializedName("Launchpad")
    @Getter
    @Setter
    private List<Member> launchpad = null;
    @Expose
    @SerializedName("CXP")
    @Getter
    @Setter
    private List<Member> cxp = null;
    @Expose
    @SerializedName("Mobile")
    @Getter
    @Setter
    private List<Member> mobile = null;

    @Getter
    private Map<String, List<Member>> departmentsMap = new HashMap<>();

    public Map<String, List<Member>> createMap() {
        departmentsMap.clear();
        departmentsMap.put("launchpad", launchpad);
        departmentsMap.put("cxp", cxp);
        departmentsMap.put("mobile", mobile);
        return departmentsMap;
    }

}
