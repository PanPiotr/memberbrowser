package com.example.memberbrowser.model;


import com.example.memberbrowser.model.data.Departments;
import com.example.memberbrowser.model.data.Member;

import java.util.List;
import java.util.Set;

import io.reactivex.Observable;


/**
 * Created by pkuszewski on 01.04.2017.
 */

public interface RepositorySource {

    Observable<Set<String>> getAllDepartments();

    Observable<List<Member>> getMembersByDepartmentName(String name);

    Observable<Departments> getAllMembers();
}
