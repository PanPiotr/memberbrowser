package com.example.memberbrowser.model;

import android.content.Context;

import com.example.memberbrowser.model.remotesource.MembersService;
import com.example.memberbrowser.model.remotesource.RemoteRepository;
import com.example.memberbrowser.model.remotesource.RemoteServiceManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

/**
 * Created by pkuszewski on 01.04.2017.
 */
@Module
public class RepositoryModule {

    @Provides
    @Singleton
    public OkHttpClient provideClient() {
        return new OkHttpClient.Builder().build();
    }

    @Provides
    @Singleton
    public Retrofit.Builder provideRetrofitBuilder() {
        return new Retrofit.Builder();
    }

    @Provides
    @Singleton
    public MembersService provideService(Context context,
                                         OkHttpClient client,
                                         Retrofit.Builder builder) {
        return new RemoteServiceManager(client, context, builder)
                .createMemberService();
    }

    @Provides
    @Singleton
    public RepositorySource provideSource(MembersService service) {
        return new RemoteRepository(service);
    }

}
