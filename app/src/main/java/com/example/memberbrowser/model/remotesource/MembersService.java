package com.example.memberbrowser.model.remotesource;

import com.example.memberbrowser.model.data.Departments;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by pkuszewski on 01.04.2017.
 */

public interface MembersService {

    @GET("members.php")
    Observable<Departments> getDepartments();

}
