package com.example.memberbrowser.model.remotesource;

import android.content.Context;

import com.example.memberbrowser.R;

import lombok.AllArgsConstructor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by pkuszewski on 01.04.2017.
 */
@AllArgsConstructor
public class RemoteServiceManager {


    private OkHttpClient httpClient;

    private Context context;

    private Retrofit.Builder builder;

    public MembersService createMemberService() {
        String url = context.getString(R.string.base_url);
        Retrofit retrofit = builder.baseUrl(url)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        return retrofit.create(MembersService.class);
    }

}
