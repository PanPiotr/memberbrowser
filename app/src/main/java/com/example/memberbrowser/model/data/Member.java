package com.example.memberbrowser.model.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Data;

/**
 * Created by pkuszewski on 01.04.2017.
 */
@Data
public class Member {

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("surname")
    private String surname;

    @Expose
    @SerializedName("email")
    private String email;

    @Expose
    @SerializedName("photo")
    private String photo;

    @Expose
    @SerializedName("role")
    private String role;
}
