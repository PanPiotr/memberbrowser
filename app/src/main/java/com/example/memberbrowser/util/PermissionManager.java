package com.example.memberbrowser.util;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

import lombok.AllArgsConstructor;

/**
 * Created by pkuszewski on 01.04.2017.
 */
@AllArgsConstructor
public class PermissionManager {

    public static final int REQUEST_CODE = 101;
    private Activity activity;

    public boolean checkWritePermission() {
        return ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    public void requestWritePermission() {
        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE);
    }

}
