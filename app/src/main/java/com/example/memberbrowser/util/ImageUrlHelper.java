package com.example.memberbrowser.util;

/**
 * Created by pkuszewski on 01.04.2017.
 */

public class ImageUrlHelper {

    private static final String BASE_URL = "http://nielsmouthaan.nl/backbase/photos/";

    public static String getUrl(String photo) {
        return BASE_URL + photo;
    }
}
