package com.example.memberbrowser.util;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;

import com.example.memberbrowser.MemberBrowserApplication;
import com.example.memberbrowser.R;
import com.example.memberbrowser.model.data.Member;
import com.example.memberbrowser.ui.memberdetails.MemberDetailsActivity;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import lombok.AllArgsConstructor;
import timber.log.Timber;

/**
 * Created by pkuszewski on 01.04.2017.
 */
@AllArgsConstructor
public class IntentManager {

    private Context context;

    public boolean sendEmail(String address) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", address, null));
        try {
            context.startActivity(Intent.createChooser(emailIntent, context.getString(R.string.send_email)));
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public void openImage(String url) {
        imageDownload(context, url);
    }

    public void openMember(Member member) {
        MemberBrowserApplication.get(context).createMemberComponent(member);
        Intent intent = new Intent(context, MemberDetailsActivity.class);
        context.startActivity(intent);
    }


    //save image
    public void imageDownload(Context ctx, String url) {
        Picasso.with(ctx)
                .load(url)
                .into(getTarget(url));
    }

    //target to save
    private Target getTarget(final String url) {
        return new Target() {

            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                Timber.d("Bitmap load started");
                new Thread(() -> {
                    Timber.d("Bitmap loaded");
                    File file = new File(Environment.getExternalStorageDirectory().getPath() + "/" + "image" + System.currentTimeMillis() + ".jpg");
                    try {
                        if (file.createNewFile()) {
                            FileOutputStream ostream = new FileOutputStream(file);
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, ostream);
                            ostream.flush();
                            ostream.close();
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setDataAndType(Uri.fromFile(file), "image/*");
                            context.startActivity(Intent.createChooser(intent, context.getString(R.string.image_chooser)));
                        }
                    } catch (IOException e) {
                        Timber.e(e.getLocalizedMessage());
                    }
                }).start();

            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
    }
}
