package com.example.memberbrowser;

/**
 * Created by pkuszewski on 01.04.2017.
 */

public interface BaseContract {

    interface View {

    }

    interface Presenter<T extends View> {

        void onViewAttached(T view);

        void onViewDetached();
    }

}
