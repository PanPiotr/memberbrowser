package com.example.memberbrowser;

import android.app.Application;
import android.content.Context;

import com.example.memberbrowser.model.data.Member;
import com.example.memberbrowser.model.member.MemberComponent;
import com.example.memberbrowser.model.member.MemberModule;

import lombok.Getter;
import timber.log.Timber;

/**
 * Created by pkuszewski on 01.04.2017.
 */

public class MemberBrowserApplication extends Application {

    @Getter
    private AppComponent appComponent;

    @Getter
    private MemberComponent memberComponent;

    public static MemberBrowserApplication get(Context context) {
        return (MemberBrowserApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
        initAppComponent();
    }

    public void createMemberComponent(Member member) {
        memberComponent = getAppComponent()
                .plus(new MemberModule(member));
    }

    public void disposeMemberComponent() {
        memberComponent = null;
    }

    private void initAppComponent() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(MemberBrowserApplication.this))
                .build();
    }
}
