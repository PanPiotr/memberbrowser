package com.example.memberbrowser.ui.memberlist.components;

import com.example.memberbrowser.scopes.ActivityScope;
import com.example.memberbrowser.ui.memberlist.modules.MemberListFragmentModule;
import com.example.memberbrowser.ui.memberlist.modules.MemberListModule;
import com.example.memberbrowser.ui.memberlist.MembersListActivity;

import dagger.Subcomponent;

/**
 * Created by pkuszewski on 01.04.2017.
 */
@ActivityScope
@Subcomponent(modules = MemberListModule.class)
public interface MemberListComponent {

    MembersListActivity inject(MembersListActivity activity);

    MemberListFragmentComponent plus(MemberListFragmentModule module);

}
