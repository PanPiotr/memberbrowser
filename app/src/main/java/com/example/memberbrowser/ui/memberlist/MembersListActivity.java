package com.example.memberbrowser.ui.memberlist;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.example.memberbrowser.BaseActivity;
import com.example.memberbrowser.MemberBrowserApplication;
import com.example.memberbrowser.R;
import com.example.memberbrowser.model.data.Member;
import com.example.memberbrowser.ui.memberlist.adapters.MembersCategoriesAdapter;
import com.example.memberbrowser.ui.memberlist.components.MemberListComponent;
import com.example.memberbrowser.ui.memberlist.modules.MemberListModule;

import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import lombok.Getter;

public class MembersListActivity extends BaseActivity implements MemberListContract.View {

    @Inject
    MemberListContract.Presenter presenter;

    @Inject
    MembersCategoriesAdapter<MemberListFragment> fragmentAdapter;

    @Getter
    private MemberListComponent activityComponent;

    @BindView(R.id.view_pager)
    ViewPager viewPager;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_members_list);
        ButterKnife.bind(this);
        setupViews();
        presenter.onViewAttached(this);
    }

    private void setupViews() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.sync));
        progressDialog.setIndeterminate(true);
        viewPager.setAdapter(fragmentAdapter);
    }

    @Override
    protected void setupComponent() {
        activityComponent = MemberBrowserApplication.get(this)
                .getAppComponent()
                .plus(new MemberListModule(this));
        activityComponent.inject(this);
    }

    @Override
    public void startLoading() {
        progressDialog.show();
    }

    @Override
    public void stopLoading() {
        progressDialog.hide();
    }

    @Override
    public void setCategories(Set<String> categories) {
        createFragments(categories);
    }

    private void createFragments(Set<String> categories) {
        int i = 0;
        for (String category : categories) {
            MemberListFragment fragment = new MemberListFragment();
            fragment.setTitle(category);
            fragment.setListener(member -> presenter.onMemberSelected(member));
            fragmentAdapter.getMapFragment().put(i, fragment);
            i++;
        }
        fragmentAdapter.notifyDataSetChanged();
    }

    @Override
    public void setMembersForCategory(String category, List<Member> members) {
        MemberListFragment fragment = fragmentAdapter.findByTitle(category);
        fragment.setItems(members);
    }

    @Override
    public void showNoConnectionError() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.connection_error_title);
        builder.setMessage(R.string.connection_error_message);
        builder.setPositiveButton(R.string.ok, null);
        builder.show();
    }
}
