package com.example.memberbrowser.ui.memberdetails;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.memberbrowser.BaseFragment;
import com.example.memberbrowser.MemberBrowserApplication;
import com.example.memberbrowser.R;
import com.example.memberbrowser.model.data.Member;
import com.example.memberbrowser.util.ImageUrlHelper;
import com.squareup.picasso.Picasso;

import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A placeholder fragment containing a simple view.
 */
public class MemberDetailsActivityFragment extends BaseFragment implements MemberDetailsContract.View {

    @Inject
    Member member;

    @Inject
    MemberDetailsContract.Presenter presenter;

    @BindView(R.id.member_avatar)
    ImageView memberAvatar;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.member_email)
    TextView memberEmail;
    @BindView(R.id.member_role)
    TextView memberRole;
    @BindView(R.id.member_name)
    TextView memberName;

    public MemberDetailsActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_member_details, container, false);
        ButterKnife.bind(this, view);
        setupViews();
        return view;
    }

    @Override
    public void onDestroyView() {
        MemberBrowserApplication.get(getActivity()).disposeMemberComponent();
        super.onDestroyView();
    }

    private void setupViews() {
        toolbar.setTitle(String.format(Locale.getDefault(), "%s %s", member.getName(), member.getSurname()));
        memberName.setText(String.format(Locale.getDefault(), "%s %s", member.getName(), member.getSurname()));
        memberEmail.setText(member.getEmail());
        memberRole.setText(member.getRole());
        memberAvatar.setOnClickListener(view -> presenter.onPictureClicked(ImageUrlHelper.getUrl(member.getPhoto())));
        Picasso.with(getActivity()).load(ImageUrlHelper.getUrl(member.getPhoto()))
                .error(R.drawable.avatar_placeholder)
                .placeholder(R.drawable.avatar_placeholder)
                .into(memberAvatar);
    }

    @Override
    protected void setupComponent() {
        MemberBrowserApplication.get(getActivity())
                .getMemberComponent()
                .plus(new MemberDetailsModule(this, getActivity()))
                .inject(this);
    }

    @OnClick({R.id.member_email, R.id.fab})
    public void onEmailClick(View view) {
        presenter.onSendEmailClicked(member.getEmail());
    }

    @Override
    public void showEmailError() {
        Snackbar.make(memberEmail, R.string.no_email_app, Snackbar.LENGTH_SHORT).show();
    }
}
