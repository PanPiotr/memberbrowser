package com.example.memberbrowser.ui.memberdetails;

import com.example.memberbrowser.BaseContract;

/**
 * Created by pkuszewski on 01.04.2017.
 */

public interface MemberDetailsContract {

    interface View extends BaseContract.View {

        void showEmailError();
    }

    interface Presenter extends BaseContract.Presenter<View> {

        void onSendEmailClicked(String email);

        void onPictureClicked(String imageUrl);

    }

}
