package com.example.memberbrowser.ui.memberlist.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.memberbrowser.R;
import com.example.memberbrowser.model.data.Member;
import com.example.memberbrowser.util.ImageUrlHelper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import lombok.Setter;

/**
 * Created by pkuszewski on 01.04.2017.
 */

public class MemberListAdapter extends RecyclerView.Adapter<MemberListAdapter.ViewHolder> {


    @Setter
    private List<Member> members = new ArrayList<>();

    @Setter
    private MemberClickedListener listener;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.member_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Member member = members.get(position);
        holder.userName.setText(String.format(Locale.getDefault(), "%s %s", member.getName(), member.getSurname()));
        Picasso.with(holder.userAvatar.getContext())
                .load(ImageUrlHelper.getUrl(member.getPhoto()))
                .placeholder(R.drawable.avatar_placeholder)
                .error(R.drawable.avatar_placeholder)
                .fit()
                .into(holder.userAvatar);
        holder.itemView.setOnClickListener(view -> {
            if (listener != null) {
                listener.onMemberClicked(member);
            }
        });
    }

    @Override
    public int getItemCount() {
        return members.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.user_avatar)
        CircleImageView userAvatar;

        @BindView(R.id.user_name)
        TextView userName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface MemberClickedListener {
        void onMemberClicked(Member member);
    }

}
