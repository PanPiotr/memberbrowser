package com.example.memberbrowser.ui.memberlist.components;

import com.example.memberbrowser.scopes.FragmentScope;
import com.example.memberbrowser.ui.memberlist.MemberListFragment;
import com.example.memberbrowser.ui.memberlist.modules.MemberListFragmentModule;

import dagger.Subcomponent;

/**
 * Created by pkuszewski on 01.04.2017.
 */
@Subcomponent(modules = MemberListFragmentModule.class)
@FragmentScope
public interface MemberListFragmentComponent {

    MemberListFragment inject(MemberListFragment fragment);
}
