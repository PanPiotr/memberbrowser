package com.example.memberbrowser.ui.memberlist.modules;


import android.support.v4.app.FragmentManager;

import com.example.memberbrowser.model.RepositorySource;
import com.example.memberbrowser.scopes.ActivityScope;
import com.example.memberbrowser.ui.memberlist.MemberListContract;
import com.example.memberbrowser.ui.memberlist.MemberListFragment;
import com.example.memberbrowser.ui.memberlist.MemberListPresenter;
import com.example.memberbrowser.ui.memberlist.MembersListActivity;
import com.example.memberbrowser.ui.memberlist.adapters.MembersCategoriesAdapter;
import com.example.memberbrowser.util.IntentManager;

import dagger.Module;
import dagger.Provides;
import lombok.AllArgsConstructor;

/**
 * Created by pkuszewski on 01.04.2017.
 */
@ActivityScope
@Module
@AllArgsConstructor
public class MemberListModule {

    private MembersListActivity activity;

    @Provides
    @ActivityScope
    FragmentManager provideFragmentManager() {
        return activity.getSupportFragmentManager();
    }

    @Provides
    @ActivityScope
    MembersCategoriesAdapter<MemberListFragment> provideAdaper(FragmentManager manager) {
        return new MembersCategoriesAdapter<>(manager);
    }

    @Provides
    @ActivityScope
    IntentManager provideIntentManager() {
        return new IntentManager(activity);
    }

    @Provides
    @ActivityScope
    MemberListContract.Presenter providePresenter(RepositorySource source, IntentManager intentManager) {
        return new MemberListPresenter(source, activity, intentManager);
    }

}
