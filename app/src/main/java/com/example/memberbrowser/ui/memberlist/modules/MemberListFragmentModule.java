package com.example.memberbrowser.ui.memberlist.modules;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.memberbrowser.scopes.FragmentScope;
import com.example.memberbrowser.ui.memberlist.MemberListFragment;
import com.example.memberbrowser.ui.memberlist.adapters.MemberListAdapter;

import dagger.Module;
import dagger.Provides;
import lombok.AllArgsConstructor;

/**
 * Created by pkuszewski on 01.04.2017.
 */

@Module
@FragmentScope
@AllArgsConstructor
public class MemberListFragmentModule {

    private MemberListFragment fragment;

    @Provides
    @FragmentScope
    MemberListAdapter provideAdapter() {
        return new MemberListAdapter();
    }

    @Provides
    @FragmentScope
    RecyclerView.LayoutManager provideLayoutManager(Context context) {
        return new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
    }

}
