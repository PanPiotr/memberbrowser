package com.example.memberbrowser.ui.memberlist;

import com.example.memberbrowser.BaseContract;
import com.example.memberbrowser.model.data.Member;

import java.util.List;
import java.util.Set;

/**
 * Created by pkuszewski on 01.04.2017.
 */

public interface MemberListContract {

    interface View extends BaseContract.View {

        void startLoading();

        void stopLoading();

        void setCategories(Set<String> categories);

        void setMembersForCategory(String category, List<Member> members);

        void showNoConnectionError();

    }

    interface Presenter extends BaseContract.Presenter<View> {

        void onCategorySelected(String category);

        void onCurrentScrollChanged(int firstVisibleItemIndex);

        void onMemberSelected(Member member);

        void refreshDataForCategory(String category);

    }

}
