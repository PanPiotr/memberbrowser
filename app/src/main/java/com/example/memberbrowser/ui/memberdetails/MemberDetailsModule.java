package com.example.memberbrowser.ui.memberdetails;


import android.app.Activity;

import com.example.memberbrowser.scopes.FragmentScope;
import com.example.memberbrowser.util.IntentManager;
import com.example.memberbrowser.util.PermissionManager;

import dagger.Module;
import dagger.Provides;
import lombok.AllArgsConstructor;

/**
 * Created by pkuszewski on 01.04.2017.
 */
@Module
@FragmentScope
@AllArgsConstructor
public class MemberDetailsModule {

    MemberDetailsContract.View view;

    Activity activity;

    @Provides
    @FragmentScope
    PermissionManager providePermissionManager() {
        return new PermissionManager(activity);
    }

    @Provides
    @FragmentScope
    MemberDetailsContract.Presenter providePresenter(IntentManager intentManager, PermissionManager permissionManager) {
        return new MemberDetailsPresenter(intentManager, permissionManager, view);
    }

    @Provides
    @FragmentScope
    IntentManager provideIntentManager() {
        return new IntentManager(activity);
    }
}
