package com.example.memberbrowser.ui.memberlist;

import com.example.memberbrowser.BaseFragment;

/**
 * Created by pkuszewski on 01.04.2017.
 */

public abstract class TitledFragment extends BaseFragment {

    public abstract String getTitle();
}
