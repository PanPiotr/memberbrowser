package com.example.memberbrowser.ui.memberdetails;

import com.example.memberbrowser.scopes.FragmentScope;

import dagger.Subcomponent;

/**
 * Created by pkuszewski on 01.04.2017.
 */
@Subcomponent(modules = MemberDetailsModule.class)
@FragmentScope
public interface MemberDetailsComponent {

    MemberDetailsActivityFragment inject(MemberDetailsActivityFragment fragment);

}
