package com.example.memberbrowser.ui.memberlist;

import com.example.memberbrowser.model.RepositorySource;
import com.example.memberbrowser.model.data.Departments;
import com.example.memberbrowser.model.data.Member;
import com.example.memberbrowser.util.IntentManager;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by pkuszewski on 01.04.2017.
 */

public class MemberListPresenter implements MemberListContract.Presenter {

    //injected fields
    private RepositorySource source;

    private MemberListContract.View view;

    private IntentManager intentManager;


    // internal fields
    private Disposable disposable;

    private Departments cachedData;

    private String currentCategory;

    private int firstVisibleItem;


    public MemberListPresenter(RepositorySource source, MemberListContract.View view, IntentManager intentManager) {
        this.source = source;
        this.view = view;
        this.intentManager = intentManager;
    }


    private void loadFreshData() {
        view.startLoading();
        disposable = source.getAllMembers()
                .subscribeOn(Schedulers.io())
                .flatMap(departments -> {
                    cachedData = departments;
                    return source.getAllDepartments();
                })
                .observeOn(AndroidSchedulers.mainThread())
                .flatMapIterable(strings -> {
                    view.setCategories(strings);
                    return strings;
                })
                .flatMap(department -> source.getMembersByDepartmentName(department)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(members -> {
                            view.setMembersForCategory(department, members);
                            return members;
                        }))
                .subscribe(
                        members -> {/*ignore, we have done everything in mapping*/},
                        throwable -> {
                            throwable.printStackTrace();
                            view.stopLoading();
                            view.showNoConnectionError();
                        }, () -> view.stopLoading());
    }

    @Override
    public void onViewAttached(MemberListContract.View view) {
        this.view = view;
        if (cachedData == null) {
            loadFreshData();
        } else {
            reloadFromCache();
        }
    }

    private void reloadFromCache() {
        view.setCategories(cachedData.getDepartmentsMap().keySet());
        disposable = Observable.fromIterable(cachedData.getDepartmentsMap().keySet())
                .subscribe(s -> view.setMembersForCategory(s, cachedData.getDepartmentsMap().get(s)));
    }

    @Override
    public void onViewDetached() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
        view = null;
    }

    @Override
    public void onCategorySelected(String category) {
        currentCategory = category;
    }

    @Override
    public void onCurrentScrollChanged(int firstVisibleItemIndex) {
        firstVisibleItem = firstVisibleItemIndex;
    }

    @Override
    public void onMemberSelected(Member member) {
        intentManager.openMember(member);
    }

    @Override
    public void refreshDataForCategory(String category) {
        source.getAllMembers()
                .flatMap(
                        departments -> {
                            cachedData = departments;
                            return source.getMembersByDepartmentName(category);
                        }
                )
                .subscribe(
                        members -> view.setMembersForCategory(category, members),
                        throwable -> {
                            throwable.printStackTrace();
                            view.showNoConnectionError();
                        });
    }
}
