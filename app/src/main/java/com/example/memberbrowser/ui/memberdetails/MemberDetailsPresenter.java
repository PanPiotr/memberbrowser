package com.example.memberbrowser.ui.memberdetails;

import com.example.memberbrowser.util.IntentManager;
import com.example.memberbrowser.util.PermissionManager;

import lombok.AllArgsConstructor;
import timber.log.Timber;

/**
 * Created by pkuszewski on 01.04.2017.
 */
@AllArgsConstructor
public class MemberDetailsPresenter implements MemberDetailsContract.Presenter {

    private IntentManager intentManager;

    private PermissionManager permissionManager;

    private MemberDetailsContract.View view;

    @Override
    public void onViewAttached(MemberDetailsContract.View view) {

    }

    @Override
    public void onViewDetached() {

    }

    @Override
    public void onSendEmailClicked(String email) {
        if (!intentManager.sendEmail(email)) {
            view.showEmailError();
        }
    }

    @Override
    public void onPictureClicked(String imageUrl) {
        Timber.d("Picture clicked");
        if (permissionManager.checkWritePermission()) {
            Timber.d("Picture clicked");
            intentManager.openImage(imageUrl);
        } else {
            Timber.d("Picture clicked");
            permissionManager.requestWritePermission();
        }
    }
}
