package com.example.memberbrowser.ui.memberlist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.memberbrowser.R;
import com.example.memberbrowser.model.data.Member;
import com.example.memberbrowser.ui.memberlist.adapters.MemberListAdapter;
import com.example.memberbrowser.ui.memberlist.modules.MemberListFragmentModule;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import lombok.Getter;
import lombok.Setter;
import timber.log.Timber;

/**
 * Created by pkuszewski on 01.04.2017.
 */

public class MemberListFragment extends TitledFragment {

    @Inject
    MemberListAdapter adapter;

    @Inject
    RecyclerView.LayoutManager manager;

    @Setter
    private MemberListAdapter.MemberClickedListener listener;

    @BindView(R.id.members_recycler)
    RecyclerView membersRecycler;

    @Setter
    @Getter
    private String title;

    private List<Member> members = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.member_list_fragment, container, false);
        ButterKnife.bind(this, view);
        setupViews();
        return view;
    }

    private void setupViews() {
        membersRecycler.setAdapter(adapter);
        membersRecycler.setLayoutManager(manager);
        adapter.setListener(listener);
        adapter.setMembers(members);
        adapter.notifyDataSetChanged();
    }

    public void setItems(List<Member> items) {
        Timber.d("setItemsCalled");
        this.members = items;
        if (adapter != null) {
            adapter.setMembers(members);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void setupComponent() {
        Timber.d("Injecting...");
        ((MembersListActivity) getActivity())
                .getActivityComponent()
                .plus(new MemberListFragmentModule(this))
                .inject(this);
        Timber.d("Inject success");
    }
}
