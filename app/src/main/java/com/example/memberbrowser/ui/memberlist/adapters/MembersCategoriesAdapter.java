package com.example.memberbrowser.ui.memberlist.adapters;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.memberbrowser.ui.memberlist.TitledFragment;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by pkuszewski on 01.04.2017.
 */

public class MembersCategoriesAdapter<T extends TitledFragment> extends FragmentPagerAdapter {

    @Setter
    @Getter
    private Map<Integer, T> mapFragment = new HashMap<>();

    public MembersCategoriesAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public T getItem(int position) {
        return mapFragment.get(position);
    }

    @Override
    public int getCount() {
        return mapFragment.keySet().size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mapFragment.get(position).getTitle();
    }

    public T findByTitle(String category) {
        for (T fragment : mapFragment.values()) {
            if (fragment.getTitle().equals(category)) {
                return fragment;
            }
        }
        return null;
    }
}
