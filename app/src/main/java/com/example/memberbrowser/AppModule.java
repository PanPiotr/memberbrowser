package com.example.memberbrowser;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import lombok.AllArgsConstructor;

/**
 * Created by pkuszewski on 01.04.2017.
 */
@Module
@AllArgsConstructor
public class AppModule {

    private MemberBrowserApplication applicationContext;

    @Provides
    @Singleton
    public Context provideContext() {
        return applicationContext;
    }

}
